package main

import (
	"os"

	"gitlab.com/alexcarol/redis"
	"gitlab.com/alexcarol/redis/port"
)

func main() {
	port.Reader(os.Stdin, os.Stdout, redis.NewThreadSafeAutoExpiringDB())
}
