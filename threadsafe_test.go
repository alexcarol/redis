package redis_test

import (
	"math/rand"
	"strconv"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/alexcarol/redis"
)

func TestNewThreadSafeDBWithParallelism(t *testing.T) {
	iterations := 3000

	wg := sync.WaitGroup{}

	// this test's purpose is to help detect possible race conditions via go's -race option

	db := redis.NewThreadSafeAutoExpiringDB()
	for i := 0; i < iterations; i++ {
		wg.Add(1)
		setName := "myzset"
		go func() {
			db.Set("one", "1")
			val, err := db.Get("one")
			require.NoError(t, err)
			require.Equal(t, "1", val)

			db.Get("two")
			db.Del("two")
			db.Del("random key")

			db.ZAdd(setName, rand.Float64(), "a")

			wg.Done()
		}()

		wg.Add(1)
		go func(i int) {
			db.SetEx("two", "2", 1)
			db.Get("two")

			db.Incr("d")
			db.Incr("e")

			db.Incr(strconv.Itoa(i))

			db.DBSize()

			wg.Done()
		}(i)

		wg.Add(1)
		go func() {
			db.ZAdd(setName, rand.Float64(), "a")
			db.ZAdd(setName, rand.Float64(), "b")

			db.ZCard(setName)

			db.ZRank(setName, "a")

			db.ZRange(setName, 0, 10)

			wg.Done()
		}()
	}

	wg.Wait()
}

func TestNewThreadSafeDBGetDoesNotCauseRaceConditionsWithExpires(t *testing.T) {
	iterations := 1000

	wg := sync.WaitGroup{}

	db := redis.NewThreadSafeAutoExpiringDB()
	db.SetEx("mykey", "abc", 1)
	for i := 0; i < iterations; i++ {
		wg.Add(1)

		go func() {
			_, err := db.Get("mykey")
			for err == nil {
				_, err = db.Get("mykey")
			}

			wg.Done()
		}()
	}

	wg.Wait()
}
