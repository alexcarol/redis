package redis_test

import (
	"testing"
	"time"

	"gitlab.com/alexcarol/redis/test"

	"github.com/stretchr/testify/require"
	"gitlab.com/alexcarol/redis"
)

func TestGetNonExistingKey(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	val, err := r.Get("nonexisting")
	require.Error(t, err)
	require.Equal(t, "", val)
}

func TestSetAndGetExistingKey(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	r.Set("mykey", "Hello")
	val, err := r.Get("mykey")
	require.NoError(t, err)
	require.Equal(t, "Hello", val)
}

func TestIncr(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "mykey"
	newVal, err := r.Incr(key)
	require.NoError(t, err)
	require.Equal(t, int64(1), newVal)

	newVal, err = r.Incr(key)
	require.NoError(t, err)
	require.Equal(t, int64(2), newVal)

	newVal, err = r.Incr(key)
	require.NoError(t, err)
	require.Equal(t, int64(3), newVal)
}

func TestSetUpdatesExistingKeys(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	r.Set("mykey", "Hello")
	r.Set("mykey", "Goodbye")
	val, err := r.Get("mykey")
	require.NoError(t, err)
	require.Equal(t, "Goodbye", val)
}

func TestSetEx(t *testing.T) {
	startTime := time.Now()
	timeProvider := &test.FakeTimeProvider{CurrentTime: startTime}
	r := redis.NewThreadSafeDBWithTimeProvider(timeProvider)

	var seconds = 129

	key := "mykey"
	originalValue := "hello"
	r.SetEx(key, originalValue, seconds)

	timeProvider.CurrentTime = startTime.Add(time.Second * time.Duration(seconds-1))
	r.ForceExpiration()

	val, err := r.Get(key)
	require.NoError(t, err)
	require.Equal(t, originalValue, val)

	timeProvider.CurrentTime = startTime.Add(time.Second * time.Duration(seconds))
	r.ForceExpiration()

	val, err = r.Get(key)
	require.Error(t, err)
	require.Equal(t, "", val)

}

func TestExpirationIsOverwrittenBySet(t *testing.T) {
	startTime := time.Now()
	timeProvider := &test.FakeTimeProvider{CurrentTime: startTime}
	r := redis.NewThreadSafeDBWithTimeProvider(timeProvider)

	var seconds = 129

	key := "mykey"
	originalValue := "hello"
	r.SetEx(key, originalValue, seconds)
	r.Set(key, originalValue)

	timeProvider.CurrentTime = startTime.Add(time.Second * time.Duration(seconds+100))
	val, err := r.Get(key)
	require.NoError(t, err)
	require.Equal(t, originalValue, val)

}

func TestExpirationIsRemovedByDel(t *testing.T) {
	startTime := time.Now()
	timeProvider := &test.FakeTimeProvider{CurrentTime: startTime}
	r := redis.NewThreadSafeDBWithTimeProvider(timeProvider)

	var seconds = 129

	key := "mykey"
	originalValue := "hello"
	r.SetEx(key, originalValue, seconds)
	r.Del(key)

	_, err := r.ZAdd(key, 123, "a")
	require.NoError(t, err)

	timeProvider.CurrentTime = startTime.Add(time.Second * time.Duration(seconds+100))

	r.ForceExpiration()

	val, err := r.ZCard(key)
	require.NoError(t, err)
	require.Equal(t, uint(1), val)

}

func TestDelRemovesKeys(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	r.Set("mykey", "Hello")
	r.Set("mykey2", "Goodbye")

	r.Del("mykey")

	val, err := r.Get("mykey")
	require.Error(t, err)
	require.Equal(t, "", val)

	val, err = r.Get("mykey2")
	require.NoError(t, err)
	require.Equal(t, "Goodbye", val)
}

func TestDelReturnsAmountOfKeysRemoved(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	r.Set("mykey", "value")

	require.Equal(t, uint(1), r.Del("mykey"))
	require.Equal(t, uint(0), r.Del("mykey2"))
}

func TestDBSizeReturnsTheRightSizeWithSetAndDel(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	r.Set("one", "")
	r.Set("two", "")
	r.Set("three", "")
	r.Set("one", "newvalue")

	r.Del("three")

	require.Equal(t, uint(2), r.DBSize())
}

func TestZAddReturnsKeysAdded(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	added, err := r.ZAdd("myzset", 25, "member1")

	require.NoError(t, err)
	require.Equal(t, uint(1), added)

	added, err = r.ZAdd("myzset", 5, "member1")
	require.NoError(t, err)
	require.Equal(t, uint(0), added)
}

func TestSortedSetOperationsFailWithKeysWithStringType(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "myzset"
	r.Set(key, "randomvalue")
	added, err := r.ZAdd(key, 25, "member1")
	require.Error(t, err)
	require.Equal(t, uint(0), added)

	card, err := r.ZCard(key)
	require.Error(t, err)
	require.Equal(t, uint(0), card)

	rank, err := r.ZRank(key, "abc")
	require.Error(t, err)
	require.Equal(t, uint(0), rank)

	zrange, err := r.ZRange(key, 1, 3)
	require.Error(t, err)
	require.Empty(t, zrange)
}

func TestGetFailsToReturnSortedSets(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	_, err := r.ZAdd("myzset", 25, "member1")
	require.NoError(t, err)

	val, err := r.Get("myzset")

	require.Error(t, err)
	require.Equal(t, "", val)
}

func TestZCardReturnsSetCardinality(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "myzset"

	cardinality, err := r.ZCard(key)
	require.NoError(t, err)
	require.Equal(t, uint(0), cardinality)

	_, err = r.ZAdd(key, 25, "member1")
	require.NoError(t, err)

	cardinality, err = r.ZCard(key)
	require.NoError(t, err)
	require.Equal(t, uint(1), cardinality)

	_, err = r.ZAdd(key, 25, "member2")
	require.NoError(t, err)

	cardinality, err = r.ZCard(key)
	require.NoError(t, err)
	require.Equal(t, uint(2), cardinality)
}

func TestZRankReturnsMemberPosition(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "myzset"

	_, err := r.ZAdd(key, 1, "one")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 2, "two")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 3, "three")
	require.NoError(t, err)

	rank, err := r.ZRank(key, "three")
	require.NoError(t, err)
	require.Equal(t, uint(2), rank)

	rank, err = r.ZRank(key, "four")
	require.Error(t, err)
	require.Equal(t, uint(0), rank)
}

func TestZAddCanInsertNewMemberWithLessScore(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "myzset"

	_, err := r.ZAdd(key, 2, "two")
	require.NoError(t, err)

	_, err = r.ZAdd(key, 1, "one")
	require.NoError(t, err)

	rank, err := r.ZRank(key, "one")
	require.NoError(t, err)
	require.Equal(t, uint(0), rank)

	rank, err = r.ZRank(key, "two")
	require.NoError(t, err)
	require.Equal(t, uint(1), rank)
}

func TestZAddUpdatesAppropiately(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "myzset"

	_, err := r.ZAdd(key, 1, "one")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 2, "two")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 3, "one")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 1, "one")
	require.NoError(t, err)

	rank, err := r.ZRank(key, "one")
	require.NoError(t, err)
	require.Equal(t, uint(0), rank)

	rank, err = r.ZRank(key, "two")
	require.NoError(t, err)
	require.Equal(t, uint(1), rank)
}

func TestZRangeReturnsCorrectRange(t *testing.T) {
	r := redis.NewThreadSafeAutoExpiringDB()

	key := "myzset"

	_, err := r.ZAdd(key, 1, "one")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 2, "two")
	require.NoError(t, err)
	_, err = r.ZAdd(key, 3, "three")
	require.NoError(t, err)

	rank, err := r.ZRange(key, 0, -1)
	require.NoError(t, err)
	require.Equal(t, []string{"one", "two", "three"}, rank)

	rank, err = r.ZRange(key, 2, 3)
	require.NoError(t, err)
	require.Equal(t, []string{"three"}, rank)

	rank, err = r.ZRange(key, -2, -1)
	require.NoError(t, err)
	require.Equal(t, []string{"two", "three"}, rank)
}
