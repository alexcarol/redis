package redis

import (
	"strconv"
	"time"
)

// InMemory represents the database without any thread-safety guarantees
type InMemory struct {
	values          map[string]interface{}
	expirationTimes map[string]time.Time
	timeProvider    TimeProvider
}

func NewInMemory() *InMemory {
	return NewInMemoryWithTimeProvider(defaultTimeProvider{})
}

func NewInMemoryWithTimeProvider(t TimeProvider) *InMemory {
	return &InMemory{
		values:          map[string]interface{}{},
		timeProvider:    t,
		expirationTimes: map[string]time.Time{},
	}
}

// TimeProvider represents an object that will return the  current time
type TimeProvider interface {
	// Now returns the current time
	Now() time.Time
}

type defaultTimeProvider struct{}

func (defaultTimeProvider) Now() time.Time {
	return time.Now()
}

// Set sets the value of a specified key
func (db *InMemory) Set(key, value string) {
	db.removeExpiration(key)
	db.values[key] = value
}

// SetEx sets the value of a key and gives it an expiration
func (db *InMemory) SetEx(key, value string, seconds int) {
	db.values[key] = value
	db.addExpiration(key, seconds)
}

// Get returns the value of a key or an error if it does not exist
func (db *InMemory) Get(key string) (string, error) {
	val, found := db.values[key]

	if !found {
		return "", ValueNotFound{}
	}

	s, ok := val.(string)
	if !ok {
		return "", incorrectKindError{}
	}

	return s, nil
}

// Del attempts removes a key and returns the amount of keys removed
func (db *InMemory) Del(key string) uint {
	if _, err := db.Get(key); err != nil {
		return 0
	}

	delete(db.values, key)
	db.removeExpiration(key)

	return 1
}

// DBSize returns the number of keys in the database
func (db *InMemory) DBSize() uint {
	return uint(len(db.values))
}

// Incr increments the number stored at key by one. If the key does not exist, it is set to 0 before performing the operation.
// An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
// if it is successful it returns the new value
func (db *InMemory) Incr(key string) (int64, error) {
	rawVal, err := db.Get(key)
	if err != nil {
		if _, ok := err.(ValueNotFound); ok {
			db.Set(key, "1")

			return 1, nil
		} else {
			return 0, err
		}
	}

	val, err := strconv.ParseInt(rawVal, 10, 64)
	if err != nil {
		return 0, err
	}

	newVal := val + 1

	db.values[key] = strconv.FormatInt(newVal, 10)

	return newVal, nil
}

// ZAdd inserts an element in a sorted set. If a specified member is already a member of the sorted set,
// the score is updated and the element reinserted at the right position to ensure the correct ordering.
// If it succeeds it returns the number of elements updated, if the key exists and has a different type it returns an error.
func (db *InMemory) ZAdd(key string, score float64, member string) (uint, error) {
	val, found := db.values[key]
	set, ok := val.(*sortedSet)
	if found {
		if !ok {
			return 0, incorrectKindError{}
		}
	} else {
		set = newSortedSet()
		db.values[key] = set
	}

	return set.add(score, member), nil
}

// ZCard returns the sorted set cardinality (number of elements) of the sorted set stored at key or 0 if the key does  not exist.
func (db *InMemory) ZCard(key string) (uint, error) {
	val, found := db.values[key]
	if !found {
		return 0, nil
	}

	set, ok := val.(*sortedSet)
	if !ok {
		return 0, incorrectKindError{}
	}

	return set.card(), nil
}

// ZRank returns the position of the member in the sorted set (the rank is 0-indexed). If the member exists it returns the rank, otherwise it returns an error.
func (db *InMemory) ZRank(key, member string) (uint, error) {
	val, found := db.values[key]
	if !found {
		return 0, nil
	}

	set, ok := val.(*sortedSet)
	if !ok {
		return 0, incorrectKindError{}
	}

	return set.rank(member)
}

// ZRange returns the specified range of elements in the sorted set stored at key.
func (db *InMemory) ZRange(key string, start, stop int) ([]string, error) {
	val, found := db.values[key]
	if !found {
		return nil, nil
	}

	set, ok := val.(*sortedSet)
	if !ok {
		return nil, incorrectKindError{}
	}

	return set.range_(start, stop), nil
}

func (db *InMemory) ForceExpiration() {
	for key := range db.expirationTimes {
		db.applyExpirationTime(key)
	}
}

func (db *InMemory) addExpiration(key string, seconds int) {
	db.expirationTimes[key] = db.timeProvider.Now().Add(time.Second * time.Duration(seconds))
}

func (db *InMemory) applyExpirationTime(key string) {
	expiration, found := db.expirationTimes[key]

	if !found {
		return
	}

	now := db.timeProvider.Now()
	if !expiration.After(now) {
		db.removeExpiration(key)
		delete(db.values, key)
	}
}

func (db *InMemory) removeExpiration(key string) {
	delete(db.expirationTimes, key)
}

type incorrectKindError struct{}

func (incorrectKindError) Error() string {
	return "operation against a key holding the wrong kind of value"
}

// ValueNotFound is returned when trying to Get a nonexistent value
type ValueNotFound struct{}

func (ValueNotFound) Error() string {
	return "value not found"
}
