package test

import "time"

type FakeTimeProvider struct {
	CurrentTime time.Time
}

func (f *FakeTimeProvider) Now() time.Time {
	return f.CurrentTime
}
