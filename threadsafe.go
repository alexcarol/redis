package redis

import (
	"sync"
	"time"
)

type threadSafe struct {
	DB

	m sync.RWMutex
}

// NewThreadSafeAutoExpiringDB
func NewThreadSafeAutoExpiringDB() DB {
	db := NewThreadSafeDB()

	go func() {
		for {
			select {
			case <-time.After(time.Millisecond * 100):
				db.ForceExpiration()
			}
		}
	}()

	return db
}

func NewThreadSafeDB() DB {
	return &threadSafe{DB: NewInMemory()}
}

func NewThreadSafeDBWithTimeProvider(provider TimeProvider) DB {
	db := NewInMemoryWithTimeProvider(provider)

	return &threadSafe{DB: db}
}

func (db *threadSafe) Set(key, value string) {
	db.m.Lock()
	defer db.m.Unlock()
	db.DB.Set(key, value)
}

func (db *threadSafe) SetEx(key, value string, seconds int) {
	db.m.Lock()
	defer db.m.Unlock()
	db.DB.SetEx(key, value, seconds)
}

func (db *threadSafe) Get(key string) (string, error) {
	db.m.RLock()
	defer db.m.RUnlock()
	return db.DB.Get(key)
}

func (db *threadSafe) DBSize() uint {
	db.m.RLock()
	defer db.m.RUnlock()

	return db.DB.DBSize()
}

func (db *threadSafe) Del(key string) uint {
	db.m.Lock()
	defer db.m.Unlock()

	return db.DB.Del(key)
}

func (db *threadSafe) Incr(key string) (int64, error) {
	db.m.Lock()
	defer db.m.Unlock()

	return db.DB.Incr(key)
}

func (db *threadSafe) ZAdd(key string, score float64, member string) (uint, error) {
	db.m.Lock()
	defer db.m.Unlock()

	return db.DB.ZAdd(key, score, member)
}

func (db *threadSafe) ZCard(key string) (uint, error) {
	db.m.RLock()
	defer db.m.RUnlock()

	return db.DB.ZCard(key)
}

func (db *threadSafe) ZRank(key, member string) (uint, error) {
	db.m.RLock()
	defer db.m.RUnlock()

	return db.DB.ZRank(key, member)
}

func (db *threadSafe) ZRange(key string, start, stop int) ([]string, error) {
	db.m.RLock()
	defer db.m.RUnlock()

	return db.DB.ZRange(key, start, stop)
}

func (db *threadSafe) ForceExpiration() {
	db.m.Lock()
	defer db.m.Unlock()

	db.DB.ForceExpiration()
}
