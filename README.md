redis
=====
How to run the project:
1. Install go 1.11 or higher
2. Run `go test ./...` to run the tests
3. Run `go run cmd/cli/main.go <sample_input.txt` to check you can run the project correctly. You should get the following output:
```
"OK"
(integer) 1
(integer) 0
(nil)
"OK"
(integer) 1
(nil)
```
4. Run the cli tool using input from the CLI and type commands as you go. Bear in mind that the CLI integration is mostly a "happy path" implementation, so it should ignore commands that are incorrect or have too many parameters.

A look at computational complexity of the operations:
Go map is a hash table, so we're assuming an average complexity of O(1) for insertion, search and delete but in high load it can go up to O(n) 

SET key value
- ME: O(1)
- REDIS: O(1) 

SET key value EX seconds
- ME: O(1)
- REDIS: O(1)

GET key
- ME: O(1)
- REDIS: O(1)

DEL key
- ME: O(1)
- REDIS: O(1)

DBSIZE
- ME: O(1)
- REDIS: doesn't say, assuming O(1)

INCR key
- ME; O(1) 
- REDIS: O(1)

ZADD key score member
- ME: O(N) with N being the number of keys in the sorted set
- REDIS: O(log(N)) where n is the number of keys in the sorted set

ZCARD key
- ME: O(1)
- REDIS: O(1)

ZRANK key member 
- ME: (log(N) + M) with N being the number of keys in the sorted set and M being the number of members with the same score as the searched member
- REDIS: O(log(N)) with N being the number of elements in the sorted set

ZRANGE key start stop
- ME: O(M) with M being the number of elements returned 
- REDIS: O(log(N)+M) with N being the number of elements in the sorted set and M the number of elements returned.
