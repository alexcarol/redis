package redis

type DB interface {
	// Set sets the value of a specified key
	Set(key, value string)

	// SetEx sets the value of a key and gives it an expiration
	SetEx(key, value string, seconds int)

	// Get returns the value of a key or an error if it does not exist
	Get(key string) (string, error)

	// Del attempts removes a key and returns the amount of keys removed
	Del(key string) uint

	// DBSize returns the number of keys in the database
	DBSize() uint

	// Incr increments the number stored at key by one. If the key does not exist, it is set to 0 before performing the operation.
	// An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
	// if it is successful it returns the new value
	Incr(key string) (int64, error)

	// ZAdd inserts an element in a sorted set. If a specified member is already a member of the sorted set,
	// the score is updated and the element reinserted at the right position to ensure the correct ordering.
	// If it succeeds it returns the number of elements updated, if the key exists and has a different type it returns an error.
	ZAdd(key string, score float64, member string) (uint, error)

	// ZCard returns the sorted set cardinality (number of elements) of the sorted set stored at key or 0 if the key does  not exist.
	ZCard(key string) (uint, error)

	// ZRank returns the position of the member in the sorted set (the rank is 0-indexed). If the member exists it returns the rank, otherwise it returns an error.
	ZRank(key, member string) (uint, error)

	// ZRange returns the specified range of elements in the sorted set stored at key.
	ZRange(key string, start, stop int) ([]string, error)

	// ForceExpiration deletes all expired keys
	ForceExpiration()
}
