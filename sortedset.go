package redis

import (
	"errors"
	"sort"
)

type sortedSet struct {
	members        map[string]float64
	orderedMembers []string
}

func newSortedSet() *sortedSet {
	return &sortedSet{
		members: map[string]float64{},
	}
}

func (s *sortedSet) add(newScore float64, member string) uint {
	oldScore, found := s.members[member]

	newRank := s.findRankScore(newScore)

	var amountModified uint
	if !found {
		amountModified = 1

		s.orderedMembers = append(s.orderedMembers, "")
		copy(s.orderedMembers[newRank+1:], s.orderedMembers[newRank:])
		s.orderedMembers[newRank] = member
	} else if len(s.orderedMembers) > 1 {
		oldRank := s.findRankScore(oldScore)

		beginning := oldRank
		end := newRank

		if oldRank > newRank {
			beginning = newRank
			end = oldRank
		}

		if oldRank == len(s.orderedMembers) {
			end = len(s.orderedMembers) - 1
		}

		if newRank == len(s.orderedMembers) {
			end = len(s.orderedMembers) - 1
		}

		for i := beginning; i < end; i++ {
			s.orderedMembers[i], s.orderedMembers[i+1] = s.orderedMembers[i+1], s.orderedMembers[i]
		}
	}

	s.members[member] = newScore

	return amountModified
}

func (s *sortedSet) card() uint {
	return uint(len(s.members))
}

func (s *sortedSet) rank(member string) (uint, error) {
	memberScore := s.members[member]
	startKey := s.findRankScore(memberScore)

	for rank := startKey; rank < len(s.orderedMembers); rank++ {
		if s.orderedMembers[rank] == member {
			return uint(rank), nil
		}
	}

	return 0, errors.New("member not found")
}

func (s *sortedSet) findRankScore(score float64) int {
	return sort.Search(len(s.orderedMembers), func(i int) bool {
		return s.members[s.orderedMembers[i]] >= score
	})
}

func (s *sortedSet) range_(start, stop int) []string {
	length := len(s.members)
	normalizedStart := normalizeStart(start, length)
	normalizedStop := normalizeStop(stop, length)

	return s.orderedMembers[normalizedStart:normalizedStop]
}

func normalizeStop(stop int, length int) int {
	var normalizedStop = stop + 1
	if stop < 0 {
		normalizedStop = length + stop + 1
	} else if normalizedStop >= length {
		normalizedStop = length
	}
	return normalizedStop
}

func normalizeStart(start, length int) int {
	var normalizedStart = start
	if start < 0 {
		normalizedStart = length + start
		if normalizedStart < 0 {
			normalizedStart = 0
		}
	}
	return normalizedStart
}
