package port

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.com/alexcarol/redis"
)

func Reader(r io.Reader, w io.Writer, db redis.DB) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()

		command := getCommand(line)

		executeCommand(w, db, command)
	}
}

type command []string

func (c command) getName() string {
	return c[0]
}

func (c command) getKey() string {
	return c[1]
}

func (c command) getStringArg(argumentNumber int) string {
	return c[argumentNumber]
}
func (c command) getFloat64Arg(argumentNumber int) (float64, error) {
	rawArgument := c[argumentNumber]

	return strconv.ParseFloat(rawArgument, 64)
}

func (c command) getIntArg(argumentNumber int) (int, error) {
	return strconv.Atoi(c[argumentNumber])
}

func getCommand(line string) command {
	args := strings.Split(line, " ")
	for i, arg := range args {
		args[i] = sanitizeArgument(arg)
	}

	return args
}

func executeCommand(w io.Writer, db redis.DB, command command) {
	switch command.getName() {
	case "GET":
		key := command.getKey()
		val, err := db.Get(key)
		if err != nil {
			if _, ok := err.(redis.ValueNotFound); ok {
				printNil(w)
			} else {
				fmt.Fprintln(w, err.Error())
			}
		} else {
			fmt.Fprintf(w, "\"%s\"\n", val)
		}
	case "SET":
		key := command.getKey()
		value := command.getStringArg(2)

		if len(command) < 5 {
			db.Set(key, value)
		} else {
			// we assume the 3rd argument is "EX"
			seconds, err := command.getIntArg(4)
			if err != nil {
				fmt.Fprintln(w, "ERR ERR value is not an integer or out of range")
			} else {
				db.SetEx(key, value, seconds)
			}
		}

		printOK(w)

	case "INCR":
		key := command.getKey()

		newValue, err := db.Incr(key)
		if err != nil {
			fmt.Println(w, err)
		} else {
			printInteger(w, newValue)
		}

	case "DBSIZE":
		size := db.DBSize()

		printUnsignedInteger(w, size)

	case "DEL":
		key := command.getKey()

		amount := db.Del(key)

		printUnsignedInteger(w, amount)

	case "ZADD":
		key := command.getKey()
		score, err := command.getFloat64Arg(2)
		if err != nil {
			fmt.Fprintln(w, IncorrectArgumentType{}.Error())
			return
		}

		member := command.getStringArg(3)
		changed, err := db.ZAdd(key, score, member)
		if err != nil {
			fmt.Fprintln(w, err.Error())
		} else {
			printUnsignedInteger(w, changed)
		}
	case "ZRANGE":
		key := command.getKey()
		start, err := command.getIntArg(2)
		if err != nil {
			fmt.Fprintln(w, IncorrectArgumentType{}.Error())
			return
		}
		stop, err := command.getIntArg(3)
		if err != nil {
			fmt.Fprintln(w, IncorrectArgumentType{}.Error())
			return
		}

		zRange, err := db.ZRange(key, start, stop)
		if err != nil {
			fmt.Fprintln(w, "error executing zrange")
		} else {
			for i, member := range zRange {
				fmt.Fprintf(w, "%d) \"%s\"\n", i+1, member)
			}
		}
	case "ZRANK":
		key := command.getKey()
		member := command.getStringArg(2)

		amount, err := db.ZRank(key, member)
		if err != nil {
			fmt.Fprintln(w, "error executing zrank")
		} else {
			printUnsignedInteger(w, amount)
		}
	case "ZCARD":
		key := command.getKey()

		amount, err := db.ZCard(key)
		if err != nil {
			fmt.Fprintln(w, "error executing zrank")
		} else {
			printUnsignedInteger(w, amount)
		}
	}

}

func printOK(w io.Writer) (int, error) {
	return fmt.Fprintln(w, `"OK"`)
}

func printNil(w io.Writer) (int, error) {
	return fmt.Fprintln(w, "(nil)")
}

type IncorrectArgumentType struct{}

func (IncorrectArgumentType) Error() string {
	return "score needs double precision"
}

func printUnsignedInteger(w io.Writer, u uint) (int, error) {
	return fmt.Fprintf(w, "(integer) %d\n", u)
}

func printInteger(w io.Writer, i int64) (int, error) {
	return fmt.Fprintf(w, "(integer) %d\n", i)
}

func sanitizeArgument(s string) string {
	if hasQuotes(s) {
		return s[1 : len(s)-1]
	}

	return s
}

func hasQuotes(s string) bool {
	return len(s) > 0 && s[0] == '"' && s[len(s)-1] == '"'
}
