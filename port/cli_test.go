package port_test

import (
	"bytes"
	"strings"
	"testing"
	"time"

	"gitlab.com/alexcarol/redis/test"

	"github.com/stretchr/testify/require"

	"gitlab.com/alexcarol/redis/port"

	"gitlab.com/alexcarol/redis"
)

func TestReader(t *testing.T) {
	tests := []struct {
		name           string
		input          string
		expectedOutput string
	}{
		{
			name: "Get and set",
			input: `GET nonexisting
SET mykey "Hello"
GET mykey
SET mykey2 Hello
GET mykey2
`,
			expectedOutput: `(nil)
"OK"
"Hello"
"OK"
"Hello"
`,
		},
		{
			name: "DBSize with strings, sorted sets while deleting stuff",
			input: `SET mykey1 1
SET mykey1 2
SET mykey2 34
DEL mykey2
ZADD myzset 1 one
ZADD myzset 2 two
DBSIZE
`,
			expectedOutput: `"OK"
"OK"
"OK"
(integer) 1
(integer) 1
(integer) 1
(integer) 2
`,
		},
		{
			name: "Incr",
			input: `SET mykey 1
INCR mykey
INCR mykey2
GET mykey
GET mykey2
`,
			expectedOutput: `"OK"
(integer) 2
(integer) 1
"2"
"1"
`,
		},
		{
			name: "Ensure lines without commands are ignored",
			input: `
SET mykey "Hello"

  
GET mykey
`,
			expectedOutput: `"OK"
"Hello"
`,
		},
		{
			name: "DEL works correctly",
			input: `
SET mykey "Hello"
DEL mykey
DEL mykey
GET mykey
SET mykey2 "Hello"
DEL "mykey2"
GET mykey
`,
			expectedOutput: `"OK"
(integer) 1
(integer) 0
(nil)
"OK"
(integer) 1
(nil)
`,
		},
		{
			name: "Sorted set commands",
			input: `
ZADD myzset 1 "one"
ZADD myzset 2 "two"
ZADD myzset 3 "three"
ZRANGE myzset 0 -1
ZRANGE myzset 2 3
ZRANK myzset "two"
ZCARD "myzset"
`,
			expectedOutput: `(integer) 1
(integer) 1
(integer) 1
1) "one"
2) "two"
3) "three"
1) "three"
(integer) 1
(integer) 3
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			reader := strings.NewReader(tt.input)

			writer := bytes.NewBuffer([]byte{})

			db := redis.NewInMemory()

			port.Reader(reader, writer, db)

			actualOutput := writer.String()

			require.Equal(t, tt.expectedOutput, actualOutput)
		})
	}
}

func TestSetEx(t *testing.T) {
	input1 := `SET mykey "Hello" EX 23
GET mykey
`
	input2 := `GET mykey`
	expectedOutput := `"OK"
"Hello"
(nil)
`

	reader := strings.NewReader(input1)

	writer := bytes.NewBuffer([]byte{})

	startTime := time.Now()
	timeProvider := &test.FakeTimeProvider{CurrentTime: startTime}

	db := redis.NewInMemoryWithTimeProvider(timeProvider)

	port.Reader(reader, writer, db)

	timeProvider.CurrentTime = startTime.Add(time.Second * 23)
	db.ForceExpiration()

	port.Reader(strings.NewReader(input2), writer, db)

	actualOutput := writer.String()
	require.Equal(t, expectedOutput, actualOutput)
}
